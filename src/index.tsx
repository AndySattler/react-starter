import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'
import { Route, Redirect, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import CharacterContainer from './containers/CharactersContainer'
import store, {history} from './store/store'
import PageFooter from './components/PageFooter'
import PageHeader from './components/PageHeader'
import ShowMessage from './components/generic/ShowMessage'
import { ThemeProvider, createMuiTheme } from '@material-ui/core'
import { ConnectedRouter } from 'connected-react-router'

const theme = createMuiTheme({
    "palette": {
        "common": {
            "black": "#000",
            "white": "#fff"
        },
        "background": {
            "paper": "#fff",
            "default": "#fafafa"
        },
        "primary": {
            "light": "rgba(205, 214, 224, 1)",
            "main": "rgba(2, 40, 81, 1)",
            "dark": "rgba(104, 132, 163, 1)",
            "contrastText": "#fff"
        },
        "secondary": {
            "light": "rgba(255, 249, 229, 1)",
            "main": "rgba(255, 191, 0, 1)",
            "dark": "rgba(255, 223, 128, 1)",
            "contrastText": "#fff"
        },
        "error": {
            "light": "#e57373",
            "main": "#f44336",
            "dark": "#d32f2f",
            "contrastText": "#fff"
        },
        "text": {
            "primary": "rgba(0, 0, 0, 0.87)",
            "secondary": "rgba(0, 0, 0, 0.54)",
            "disabled": "rgba(0, 0, 0, 0.38)",
            "hint": "rgba(0, 0, 0, 0.38)"
        }
    }
})

const routing = (
    <ThemeProvider theme={theme}>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <PageHeader />
                <Box display="flex" flexDirection="column">
                    <Box minHeight="87vh">
                        <Grid container>
                            <Grid item sm={1} />
                            <Grid item sm={10}>
                                <Switch>
                                    <Route path="/characters" component={CharacterContainer} />                                        
                                    <Redirect to='/characters' />
                                </Switch>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box>
                        <PageFooter />
                    </Box>
                </Box>
                <ShowMessage />
            </ConnectedRouter>
        </Provider>
    </ThemeProvider>
)

ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
