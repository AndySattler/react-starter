import { 
    put, 
    takeEvery 
} from 'redux-saga/effects'

import {
   SHOW_MESSAGE
} from './reducers/applicationSettingsReducer'

import {
    ICharacterResponse
} from './types'

import {
    GetCharacters,
} from './api'

import { GetCharactersBeginAction, GET_CHARACTERS_BEGIN, GET_CHARACTERS_SUCCESS } from './reducers/characterReducer'

function* getCharacters({payload: undefined} : GetCharactersBeginAction) {
    try {       
        const characterResponse : ICharacterResponse = yield GetCharacters()
    
        yield put({ type: GET_CHARACTERS_SUCCESS, payload: characterResponse })
    }
    catch (e) {
        yield put({
            type: SHOW_MESSAGE, 
            payload: {
                messageType: 'error',
                open: true, message:
                    `Error Loading Action Details ${e}`
            }
        })
    }
}


function* mySaga() {
    yield takeEvery(GET_CHARACTERS_BEGIN, getCharacters)   
}

export default mySaga;