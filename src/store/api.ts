import { BASE_URL } from '../constants'
import { 
    ICharacterResponse,
 } from './types'

 import * as fetch from './fetch'

export async function GetCharacters() {
    const url = BASE_URL + `character`

    const response = await fetch.get<ICharacterResponse>(url)
        .then(res => { return res })

    return response
}

