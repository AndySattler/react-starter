import {    
    ICharacter,
    ICharacterResponse,
} from '../types'

//Action Types
export const GET_CHARACTERS_BEGIN = 'GET_CHARACTERS_BEGIN'
export const GET_CHARACTERS_SUCCESS = 'GET_CHARACTERS_SUCCESS'

export interface GetCharactersBeginAction {
    type: typeof GET_CHARACTERS_BEGIN,
    payload: undefined
}

export interface GetCharactersSuccessAction {
    type: typeof GET_CHARACTERS_SUCCESS,
    payload: ICharacterResponse
}

export type ActionTypes =
    GetCharactersBeginAction |
    GetCharactersSuccessAction

//Action Creators
export function getCharacters(): ActionTypes {
    return {
        type: GET_CHARACTERS_BEGIN,
        payload: undefined
    }
}

export interface ICharactersState {
    characters: ICharacter[],
    isLoading: boolean,
}

export const charactersInitialState: ICharactersState = {
    characters: [],
    isLoading: false,
}

//reducer
export const characterStateReducer = (state: ICharactersState = charactersInitialState, action: ActionTypes, ) => {
    switch (action.type) {
        case GET_CHARACTERS_BEGIN:
            return {
                ...state, 
                isLoading: true, 
            }

        case GET_CHARACTERS_SUCCESS:          
            return {
                ...state, 
                isLoading: false,
                characters: action.payload.results
            }        

        default:
            return { ...state }
    }
}
