import {
    IMessage,
} from '../types'

export const SHOW_MESSAGE = 'SHOW_MESSAGE'
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE'

export interface ShowMessageAction {
    type: typeof SHOW_MESSAGE,
    payload: IMessage
}

export interface CloseMessageAction {
    type: typeof CLOSE_MESSAGE,
    payload: undefined
}

export type ApplicationSettingsActionTypes =
    ShowMessageAction |
    CloseMessageAction

export function showMessage(snackbar: IMessage): ApplicationSettingsActionTypes {
    return {
        type: SHOW_MESSAGE,
        payload: snackbar
    }
}

export interface IApplicationSettingsState {
    message: IMessage,
}

export const applicationSettingsInitialState: IApplicationSettingsState = {
    message: { messageType: 'info', open: false, message: '' },
}

export function closeMessage(): ApplicationSettingsActionTypes {
    return {
        type: CLOSE_MESSAGE,
        payload: undefined
    }
}

//reducer
export const applicationSettingsStateReducer = (state: IApplicationSettingsState = applicationSettingsInitialState, action: ApplicationSettingsActionTypes) => {
    switch (action.type) {
       
        case SHOW_MESSAGE:
            return {
                ...state, 
                message: action.payload
            }

        case CLOSE_MESSAGE:
            return {
                ...state, 
                message: {
                    open:false,
                    message:'',
                    messageType: 'error'
                }
            }

        default:
            return { ...state };
    }
}
