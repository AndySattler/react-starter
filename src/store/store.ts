import { createStore, combineReducers, applyMiddleware} from 'redux'
import { characterStateReducer, ICharactersState } from './reducers/characterReducer'
import {applicationSettingsStateReducer, IApplicationSettingsState} from './reducers/applicationSettingsReducer'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import mySaga from './sagas'
import { connectRouter } from "connected-react-router"
import { createBrowserHistory } from 'history'
import { History } from 'history'
import { routerMiddleware } from 'connected-react-router'

const sagaMiddleware = createSagaMiddleware()

export interface AppState {
    characterStateReducer : ICharactersState,
    applicationSettingsStateReducer : IApplicationSettingsState,
    router : History
}

const rootReducer = (history : History) => combineReducers({
    characterStateReducer, 
    applicationSettingsStateReducer,
    router: connectRouter(history)
})

const initialState = {}
export const history = createBrowserHistory()
const routerMw = routerMiddleware(history)

const store = createStore(rootReducer(history), initialState, applyMiddleware(logger, routerMw, sagaMiddleware))

sagaMiddleware.run(mySaga)
export default store;