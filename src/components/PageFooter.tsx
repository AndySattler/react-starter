import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import { Theme } from '@material-ui/core'
import {SUPPORT_EMAIL} from '../constants'

const useStyles = makeStyles<Theme>((theme: Theme) => ({
    footer: {
        width: '100%',
        color: theme.palette.common.black,
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(1),
    },
}));

export default function PageFooter() {
    const classes = useStyles();

    return (
        <footer className={classes.footer}>
            <Container maxWidth="sm">
                Contact us with questions or comments at <a href={`mailto:${SUPPORT_EMAIL}`}>{`${SUPPORT_EMAIL}`}</a>
                <Typography>Copyright &copy; The Regents of the University of California</Typography>
            </Container>
        </footer>
    );
}