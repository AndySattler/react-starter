import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { 
    ICharacter
} from '../store/types'
import { Theme } from '@material-ui/core'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import GridListTileBar from '@material-ui/core/GridListTileBar'
import ListSubheader from '@material-ui/core/ListSubheader'

export interface ICharactersProps {
    characters: ICharacter[],
}

const Characters: React.FC<ICharactersProps> = (props) => {
    const useStyles = makeStyles<Theme>((theme: Theme) => ({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
            overflow: 'hidden',
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
          },
          gridList: {
            flexWrap: 'wrap',
            // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
            transform: 'translateZ(0)',
          },
          tileSize: {
            width:'500px',
          },
          imageSize: {
            width:'300px',
            height:'300px'
          }
    }))

    const classes = useStyles();

    return (
    <div className={classes.root}>
      <GridList cellHeight={'auto'} className={classes.tileSize} spacing={10}>
        <GridListTile key="Subheader" style={{ height: 'auto' }}>
          <ListSubheader component="div">Characters</ListSubheader>
        </GridListTile>
        {props.characters.map((character) => (
          <GridListTile key={character.id}>
            <img className={classes.imageSize}  src={character.image} alt={character.name} />
            <GridListTileBar
              title={character.name}
              subtitle={<span>status: {character.status}</span>}            
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
    )
}

export default Characters