import React from 'react'
import Loader from 'react-loader-spinner'
import Grid from '@material-ui/core/Grid'
import { Typography } from '@material-ui/core'

interface ILoadingProps {
}

const Loading: React.FC<ILoadingProps> = (props) => {    
  return (
    <Grid container direction="row" alignItems="center" justify="center">
      <Grid item xs={4}>
        <Grid container direction="column" alignItems="center" justify="center">
          <Grid item>
            <Loader
              type="TailSpin"
              color="#00BFFF"
              height={100}
              width={100}
              timeout={0}
            />
          </Grid>
          <Grid item>
            <Typography variant="h2">Loading</Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
} 

export default Loading;
