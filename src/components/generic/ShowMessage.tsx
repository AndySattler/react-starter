import React from 'react'
import { Snackbar } from '@material-ui/core'
import { Alert, AlertTitle, } from '@material-ui/lab'
import { connect, useDispatch } from "react-redux"
import { IMessage } from '../../store/types'
import { 
    ApplicationSettingsActionTypes,
    closeMessage, 
    CLOSE_MESSAGE,      
} from '../../store/reducers/applicationSettingsReducer'
import { AppState } from '../../store/store'
import { Dispatch } from "redux"
import Typography from '@material-ui/core/Typography'

interface IErrorProps {
    message: IMessage
}

const ShowMessage: React.FC<IErrorProps> = (props) => {
    const dispatch = useDispatch();

    const callCloseSnackbar = () => {
        dispatch(closeMessage());
    }

    return (
        <>
            {props.message &&
                <Snackbar open={props.message.open} autoHideDuration={6000} onClose={callCloseSnackbar} >
                    <Alert onClose={callCloseSnackbar} severity={props.message.messageType}>
                        <AlertTitle>{props.message.messageType}</AlertTitle>
                        {props.message.message}
                        {props.message.messageType === "error" && <Typography>contact  for support</Typography>}
                    </Alert>
                </Snackbar>
            }
        </>
  );
} 

const MapStateToProps = (state: AppState) => ({
    message: state.applicationSettingsStateReducer.message,
});

const MapDispatchToProps = (dispatch: Dispatch<ApplicationSettingsActionTypes>) => ({
    closeMessage: () => dispatch({ type: CLOSE_MESSAGE, payload: undefined }),
});

export default connect(
    MapStateToProps,
    MapDispatchToProps
)(ShowMessage);