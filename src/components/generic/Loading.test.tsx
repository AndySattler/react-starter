import React from 'react'
import Loading from './Loading'
import renderer from 'react-test-renderer'

it('Loading component snapshot matches', () => {
    const component = renderer.create(
        <Loading />
    )
    expect(component).toMatchSnapshot();
})