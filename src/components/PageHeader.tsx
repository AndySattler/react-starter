import React from 'react'
import Typography from '@material-ui/core/Typography'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { makeStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core'

const useStyles = makeStyles<Theme>((theme: Theme) => ({
    toolbarButtons: {
        marginLeft: 'auto',
        color: theme.palette.common.white
    },
    searchButton: {
        paddingRight: theme.spacing(2),
        color: theme.palette.common.white
    }
}));

export default function PageHeader() {
  const classes = useStyles()
  return (
      <AppBar position="static">
          <Toolbar>                       
              <Typography variant="h6" className={classes.title}>
                 React Starter Kit
              </Typography>              
          </Toolbar>
      </AppBar>
  );
}