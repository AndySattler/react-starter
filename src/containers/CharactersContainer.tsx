import React, { useEffect } from "react"
import { connect, useDispatch } from "react-redux"
import { Dispatch } from "redux"
import { 
    ICharacter,
 } from '../store/types'
import Characters from "../components/Characters"
import { 
    ActionTypes, 
    getCharacters, 
    GET_CHARACTERS_BEGIN,
} from "../store/reducers/characterReducer"
import Loading from '../components/generic/Loading'
import { makeStyles } from '@material-ui/core/styles'
import { Theme } from '@material-ui/core'
import { AppState } from '../store/store'
import { red } from "@material-ui/core/colors"

export interface ICharactersContainerProps {
    characters: ICharacter[],
    isLoading: boolean
}

const CharactersContainer: React.FC<ICharactersContainerProps> = (props) => {

    const dispatch = useDispatch();

    const useStyles = makeStyles<Theme>((theme: Theme) => ({
        bottomContainerPadding: {
            padding: theme.spacing(2),
        },
        cannotSubmit: {
            color: red[300]
        }
    }))

    const classes = useStyles();

    useEffect(() => {
        dispatch(getCharacters())
    }, [])

    return (
        <div className={classes.bottomContainerPadding}>
            {
                !props.isLoading &&
                <Characters characters={props.characters} />
            }    
            {
                props.isLoading &&
                <Loading />
            }
        </div>
    )
}

const MapStateToProps = (state: AppState) => ({    
    characters: state.characterStateReducer.characters,
    isLoading: state.characterStateReducer.isLoading
});

const MapDispatchToProps = (dispatch: Dispatch<ActionTypes>) => ({
    getCharacters: () => dispatch({ type: GET_CHARACTERS_BEGIN, payload: undefined }),
});

export default connect(
    MapStateToProps,
    MapDispatchToProps
)(CharactersContainer)