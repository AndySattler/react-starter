## About
This is a starter kit for creating a react front end application with redux material UI, redux saga and typescript

## Setup
*Make sure you have nodejs installed
*Run `npm install` to install all of the dependencies
*Run `npm start` to start the server

## Environments
*The app will run on either the environment variable setup as REACT_APP_APIURL or default to 'http://localhost:3002/';


